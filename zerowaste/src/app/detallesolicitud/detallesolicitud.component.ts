import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { Recogida } from '../model/Recogida';
import { Articulo } from '../model/Articulo';
import { SolicitudService } from '../services/solicitud.service';
import { ArticuloService } from '../services/articulo.service';

@Component({
  selector: 'app-detallesolicitud',
  templateUrl: './detallesolicitud.component.html',
  styleUrls: ['./detallesolicitud.component.css']
})
export class DetallesolicitudComponent implements OnInit {

  mySelectedRecogida: Recogida;
  @Input() misArticulos: Array<Articulo>;
  idrecogida: string;
  miRecogida: Recogida;
  
  constructor(private router: Router, private articuloService: ArticuloService, private recogidaService: SolicitudService, private route: ActivatedRoute) { 
    this.idrecogida = this.route.snapshot.paramMap.get('id');

  }

  ngOnInit() {
    this.articuloService.getAllArticulosRecogida(this.idrecogida).subscribe(resp => {
      console.log('Viendo solicitud: ' + this.idrecogida);
      this.misArticulos = resp;
    })

    this.recogidaService.getRecogidaById(this.idrecogida).subscribe(resp => {
      console.log('Viendo solicitud: ' + this.idrecogida);
      this.mySelectedRecogida = resp;
      this.miRecogida = this.mySelectedRecogida;
    })
  }
  addArticulo() {
    this.router.navigate(['/addarticulo', this.idrecogida]);
  }

  terminar(){
    this.router.navigate(['/dashboard']);
  }
  public deleteArticulo(idarticulo: number) {
    this.articuloService.deleteArticulo(idarticulo).subscribe();
    alert("Artículo eliminado")
    location.reload();
  }

}
