import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Usuario } from '../model/Usuario';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-edit-donador',
  templateUrl: './edit-donador.component.html',
  styleUrls: ['./edit-donador.component.css']
})
export class EditDonadorComponent implements OnInit {


  editUsuario: Usuario;
  @Input() idDonador: number;
  actualizar: string;

  constructor(private router: Router, private route: ActivatedRoute, private usuarioService: UsuarioService) { }

  ngOnInit() {
    // this.idDonador = this.route.snapshot.paramMap.get('id');

    this.usuarioService.getUserById(this.idDonador).subscribe(local => {
      this.editUsuario = local;
      console.log('usuario x ID');
    });
  }

  saveDonador(correo: string, password: string, nombre: string, apellido: string, celular: number, cedula: string, fechaNacimiento: string) {

    this.usuarioService.updateDonador(this.idDonador.toString(), nombre, apellido, correo, celular.toString(), cedula, password, fechaNacimiento).subscribe(updateRecolector => {
      this.actualizar = updateRecolector;
      console.log('actualizar donador');
    });
  }
}
