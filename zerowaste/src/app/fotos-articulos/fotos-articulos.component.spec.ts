import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FotosArticulosComponent } from './fotos-articulos.component';

describe('FotosArticulosComponent', () => {
  let component: FotosArticulosComponent;
  let fixture: ComponentFixture<FotosArticulosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FotosArticulosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FotosArticulosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
