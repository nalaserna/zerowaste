import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformeRecolectorComponent } from './informe-recolector.component';

describe('InformeRecolectorComponent', () => {
  let component: InformeRecolectorComponent;
  let fixture: ComponentFixture<InformeRecolectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformeRecolectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformeRecolectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
