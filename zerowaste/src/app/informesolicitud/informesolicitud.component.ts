import { Component, OnInit, Input } from '@angular/core';
import { Usuario } from '../model/Usuario';
import { UsuarioService } from '../services/usuario.service';
import { Recogida } from '../model/Recogida';
import { SolicitudService } from '../services/solicitud.service';
import { Router } from '@angular/router';
import { elementContainerEnd, elementStyleProp } from '@angular/core/src/render3';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-informesolicitud',
  templateUrl: './informesolicitud.component.html',
  styleUrls: ['./informesolicitud.component.css']
})
export class InformesolicitudComponent implements OnInit {

  status: string;
  fechacreacion: string;
  recolectores: Array<Usuario>;
  donadores: Array<Usuario>;
  myRecolector: Usuario;
  myDonador: Usuario;
  @Input() misRecogidas: Array<Recogida>;

  recogidas: Recogida[];
  DonadorSelect = new FormControl('', [Validators.required]);

  constructor(private router: Router, private userService: UsuarioService, private recogidaService: SolicitudService) {
this.myRecolector = new Usuario();
this.myDonador = new Usuario();

  }

  ngOnInit() {
    this.recogidaService.getAllSolicitudes().subscribe(resp => {
      console.log('Viendo todas las solicitudes');
      this.misRecogidas = resp;
    })

    this.userService.getAllRecolectores().subscribe(resp => {
      console.log('Recolectores ');
      this.recolectores = resp;
    })

    this.userService.getAllDonadores().subscribe(resp => {
      console.log('Donadores ');
      this.donadores = resp;
    })
  }

  public verRecogida(id: number) {
    this.router.navigate(['/versolicitud', id]);
  }

  public deleteRecogida(id: number) {
    this.recogidaService.deleteRecogida(id + '').subscribe();
    alert("Recogida eliminada")
    location.reload();
  }

  Consultar(recolectorId: number, donadorId: number) {
    let valor = '';
    console.log("Recolector: " +recolectorId);
    console.log("DOnador: "+donadorId);
  
    this.recogidas = this.misRecogidas;

    

  }

}
