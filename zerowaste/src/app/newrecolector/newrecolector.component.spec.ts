import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewrecolectorComponent } from './newrecolector.component';

describe('NewrecolectorComponent', () => {
  let component: NewrecolectorComponent;
  let fixture: ComponentFixture<NewrecolectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewrecolectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewrecolectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
