import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Localidad } from '../model/Localidad';
import { LocalidadService } from '../services/localidad.service';
import { Usuario } from '../model/Usuario';
import { FormBuilder, FormGroup, FormControl, FormControlName } from '@angular/forms';
import { UsuarioService } from '../services/usuario.service';
import { FundacionService } from '../services/fundacion.service';
import { Fundacion } from '../model/Fundacion';


@Component({
  selector: 'app-perfilrecolector',
  templateUrl: './perfilrecolector.component.html',
  styleUrls: ['./perfilrecolector.component.css']
})
export class PerfilrecolectorComponent implements OnInit {

  fundaciones: Array<Fundacion>;
  localidades: Array<Localidad>;
  verRecolector: Usuario;
  formularioadicionperfilrecolector: FormGroup;
  visible: string;
  actualizar: string;
  @Input() recolectorid: number;


  constructor(private router: Router, private route: ActivatedRoute,
    private localidadService: LocalidadService, private fundacionService: FundacionService, private fb: FormBuilder, private usuarioService: UsuarioService) { }

  ngOnInit() {
    // this.visible = '0';
    this.recolectorid = this.route.snapshot.params['id'];

    this.localidadService.getAllLocalidades().subscribe(resp => {
      this.localidades = resp;
      console.log(this.localidades);
    });

    this.fundacionService.getAllFundaciones().subscribe(resp => {
      this.fundaciones = resp;
      console.log(this.fundaciones);
    });

    this.usuarioService.getUserById(this.recolectorid).subscribe(local => {
      this.verRecolector = local;
      console.log('usuario x ID');
    });

  }

  actualizarRecolector(nombre: string, fechaNacimiento: string, fundacion_id: Number, localidad_id: Number, apellido: string) {
    // this.newusuario.localidad = this.localidadusuario
    this.usuarioService.updateRecolector(this.recolectorid.toString(), nombre,
      fechaNacimiento, fundacion_id, localidad_id, apellido).subscribe(updateRecolector => {
        this.actualizar = updateRecolector;
        
      });
      alert("Recolector actualizado")
        location.reload();
  }

}
