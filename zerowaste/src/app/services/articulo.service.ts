import { Articulo } from './../model/Articulo';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArticuloService {

  constructor(private http: HttpClient) { }

  public getAllArticulos(): Observable<Articulo[]> {
    return this.http.get<Articulo[]>(environment.urlgetAllArticulos);
  }


  public getAllArticulosRecogida(id_Recogida: string): Observable<Articulo[]> {
    const body = new HttpParams().set('id_Recogida', id_Recogida+'');
    console.log('id_recogida' +id_Recogida);
    return this.http.post<Articulo[]>(environment.urlgetAllArticulosRecogida, body);
  }

  public addNewArticulo(id_Recogida: number, nombre: string, cantidad: number, estadoUtil: string, alto: number, ancho: number, largo: number, peso: number, descripcion: string, imagen1: string, imagen2: string): Observable<string> {

    const parametros = new HttpParams()
      .set('id_Recogida', id_Recogida.toString())
      .set('nombre', nombre)
      .set('cantidad', cantidad.toString())
      .set('estadoUtil', estadoUtil.toString())
      .set('alto', alto.toString())
      .set('ancho', ancho.toString())
      .set('largo', largo.toString())
      .set('peso', peso.toString())
      .set('descripcion', descripcion)
      .set('imagen1', imagen1)
      .set('imagen2', imagen2);

    const httpParams = parametros;

    return this.http.post<string>(environment.urladdNewArticulo, httpParams);
  }

  public deleteArticulo(id: number): Observable<string> {
    const body = new HttpParams().set('id', id+'');
    return this.http.post<string>(environment.urldeleteArticulo, body);
  }

  public getArticuloById(id: number): Observable<Articulo> {
    const body = new HttpParams().set('id', id+'');
    return this.http.post<Articulo>(environment.urlgetArticuloById, body);
  }

}