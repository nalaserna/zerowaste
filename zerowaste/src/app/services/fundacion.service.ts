import { Fundacion } from './../model/Fundacion';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FundacionService {

  constructor(private http: HttpClient) { }

  public getAllFundaciones(): Observable<Fundacion[]> {
    return this.http.get<Fundacion[]>(environment.urlgetAllFundaciones);
  }

  public addFundacion(nombre: string, direccion: string, nit: string): Observable<string> {


    const parametros = new HttpParams()
      .set('nombre', nombre)
      .set('direccion', direccion)
      .set('nit', nit);
    const httpParams = parametros;

    return this.http.post<string>(environment.urladdFundacion, httpParams);
  }

  public getdeleteFundacion(id: number): Observable<Fundacion> {
    const body = new HttpParams().set('id', id+'');
    return this.http.post<Fundacion>(environment.urldeleteFundacion, body);
  }

  public getFundaciondById(id: number): Observable<Fundacion> {
    const body = new HttpParams().set('id', id+'');
    return this.http.post<Fundacion>(environment.urlgetFundaciondById, body);
  }

  public getFundaciondByNombre(nombre: string): Observable<Fundacion> {
    const body = new HttpParams().set('nombre', nombre+'');
    return this.http.post<Fundacion>(environment.urlgetFundaciondById, body);
  }




}