import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Usuario } from '../model/Usuario';
import { NumberSymbol } from '@angular/common';
import { Fundacion } from '../model/Fundacion';
import { Localidad } from '../model/Localidad';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) { }

  public getAllUsers(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(environment.urlgetAllUsers);
  }

  public addNewDonador( nombre: string, apellido: string, email: string, celular: string,
                        cedula: string, password: string, fecha_nacimiento: string): Observable<string> {

    const parametros = new HttpParams()
      .set('nombre', nombre + '')
      .set('apellido', apellido + '')
      .set('email', email + '')
      .set('celular', celular + '')
      .set('cedula', cedula + '')
      .set('password', password + '')
      .set('fecha_nacimiento', fecha_nacimiento + '');
    const httpParams = parametros;

    return this.http.post<string>(environment.urladdNewDonador, httpParams);
  }

  public updateDonador( id: string, nombre: string, apellido: string, email: string, celular: string,
                        cedula: string, password: string, fecha_nacimiento: string): Observable<string> {

    const parametros = new HttpParams()
      .set('id', id + '')
      .set('nombre', nombre + '')
      .set('apellido', apellido + '')
      .set('email', email + '')
      .set('celular', celular + '')
      .set('cedula', cedula + '')
      .set('password', password + '')
      .set('fecha_nacimiento', fecha_nacimiento + '');
    const httpParams = parametros;

    return this.http.post<string>(environment.urlupdateDonador, httpParams);
  }


  public deleteDonador(id: number): Observable<string> {
    const body = new HttpParams().set('id', id + '');
    return this.http.post<string>(environment.urldeleteDonador, body);
  }

  public getAllRecolectores(): Observable<Usuario[]>{
    return this.http.get<Usuario[]>(environment.urlgetAllRecolectores);
  }

  public addNewRecolector(nombre: string, fecha_nacimiento: string, fundacion: Number,
                          localidad: Number, apellido: string): Observable<string> {

    const parametros = new HttpParams()
      .set('nombre', nombre)
      .set('fecha_nacimiento', fecha_nacimiento)
      .set('id_fundacion', fundacion.toString() + '')
      .set('id_localidad', localidad.toString() + '')
      .set('apellido', apellido);
    const httpParams = parametros;

    return this.http.post<string>(environment.urladdNewRecolector, httpParams);
  }

  public updateRecolector(id: string, nombre: string, fecha_nacimiento: string,
                          id_fundacion: Number, id_localidad: Number, apellido: string): Observable<string> {

    const parametros = new HttpParams()
      .set('id', id + '')
      .set('nombre', nombre)
      .set('fecha_nacimiento', fecha_nacimiento)
      .set('id_fundacion', id_fundacion.toString() + '')
      .set('id_localidad', id_localidad.toString() + '')
      .set('apellido', apellido);
    const httpParams = parametros;

    return this.http.post<string>(environment.urlupdateRecolector, httpParams);
  }

  public deleteRecolector(id: number): Observable<string> {
    const body = new HttpParams().set('id', id + '');
    return this.http.post<string>(environment.urldeleteRecolector, body);
  }

  public getAllDonadores(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(environment.urlgetAllDonadores);
  }

  public getAllRecolectoresByFundacion(id_fundacion: string): Observable<Usuario[]> {
    const body = new HttpParams().set('id_fundacion', id_fundacion);
    return this.http.post<Usuario[]>(environment.urlgetAllRecolectoresByFundacion, body);
  }

  public getAllRecolectoresLocalidad(id_localidad: number): Observable<Usuario[]> {
    const body = new HttpParams().set('id_localidad', id_localidad + '');
    return this.http.post<Usuario[]>(environment.urlgetAllRecolectoresLocalidad, body);
  }

  public getAllRecolectoresFundacionLocalidad(id_localidad: Number, id_fundacion: Number): Observable<Usuario[]> {
    const body = new HttpParams().set('id_localidad', id_localidad + '').set('id_fundacion', id_fundacion + '');
    return this.http.post<Usuario[]>(environment.urlgetAllRecolectoresFundacionLocalidad, body);
  }

  public getUserById(id: number): Observable<Usuario> {
    const body = new HttpParams().set('id', id + '');
    return this.http.post<Usuario>(environment.urlgetUserById, body);
  }

  public getDonadorByEmail(email: string): Observable<Usuario[]> {
    const body = new HttpParams().set('email', email + '');
    return this.http.post<Usuario[]>(environment.urlgetDonadorByEmail, body);
  }

}
