import { Component, OnInit } from '@angular/core';
import { Recogida } from '../model/Recogida';
import { Usuario } from '../model/Usuario';
import { SolicitudService } from '../services/solicitud.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-solicitudespendientes',
  templateUrl: './solicitudespendientes.component.html',
  styleUrls: ['./solicitudespendientes.component.css']
})
export class SolicitudespendientesComponent implements OnInit {

  misRecogidas: Array<Recogida>;
  miRecogida: Recogida;
  user: Usuario;

  constructor(private solicitudService: SolicitudService, private router: Router) {
    this.miRecogida = new Recogida();
    this.user = new Usuario();
    solicitudService.getSolicitudesPendientes().subscribe(resp => {
      this.misRecogidas = resp;
      console.log('Solicitudes pendientes consultadas');
    });
   }

  ngOnInit() {
  }

  verDetalle(recogida: number) {
    this.router.navigate(['/versolicitud', recogida]);
  }

}
