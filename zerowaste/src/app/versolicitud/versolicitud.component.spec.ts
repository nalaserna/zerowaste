import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VersolicitudComponent } from './versolicitud.component';

describe('VersolicitudComponent', () => {
  let component: VersolicitudComponent;
  let fixture: ComponentFixture<VersolicitudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VersolicitudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersolicitudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
