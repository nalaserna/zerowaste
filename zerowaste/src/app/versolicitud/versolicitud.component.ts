import { Component, OnInit, Input } from '@angular/core';
import { Recogida } from '../model/Recogida';
import { Usuario } from '../model/Usuario';
import { Router, ActivatedRoute } from '@angular/router';
import { SolicitudService } from '../services/solicitud.service';
import { Articulo } from '../model/Articulo';
import { ArticuloService } from '../services/articulo.service';
import { LocalidadService } from '../services/localidad.service';
import { Localidad } from '../model/Localidad';
import { UsuarioService } from '../services/usuario.service';


@Component({
  selector: 'app-versolicitud',
  templateUrl: './versolicitud.component.html',
  styleUrls: ['./versolicitud.component.css'],
  

})
export class VersolicitudComponent implements OnInit {
  mySelectedRecogida: Recogida;
  @Input() misArticulos: Array<Articulo>;
  idrecogida: string;
  miRecogida: Recogida;
  localidades: Array<Localidad>;
  recolectores: Array<Usuario>;
  id_localidad:number;
  activo : string;

  constructor(private localidadService: LocalidadService, private recolectorService: UsuarioService, private router: Router, private route: ActivatedRoute, private api: SolicitudService, private articuloService: ArticuloService) {

  }

  ngOnInit() {

    let activo = window.localStorage.getItem('idUsuario');
    let rol = window.localStorage.getItem('rol');
    console.log(activo);
    console.log(rol);

    if (activo == null) {
      this.router.navigateByUrl('/login');
    }


    this.localidadService.getAllLocalidades().subscribe(resp => {
      this.localidades = resp;
      console.log(this.localidades);
    });

    this.idrecogida = this.route.snapshot.paramMap.get('id');

    this.articuloService.getAllArticulosRecogida(this.idrecogida).subscribe(resp => {
      console.log('Viendo solicitud: ' + this.idrecogida);
      this.misArticulos = resp;
    })

    this.recolectorService.getAllRecolectores().subscribe(resp => {
      console.log('Recolectores ');
      this.recolectores = resp;
    })

    this.api.getRecogidaById(this.idrecogida).subscribe(resp => {
      console.log('Viendo solicitud: ' + this.idrecogida);
      this.mySelectedRecogida = resp;
      this.miRecogida = this.mySelectedRecogida;
    })
    let fechaEstimadaEntrega: any = this.miRecogida.fechaEstimadaEntrega;
  }

  public deleteArticulo(idarticulo: number) {
    this.articuloService.deleteArticulo(idarticulo).subscribe();
    alert("Artículo eliminado")
    location.reload();
  }

  public asignarRecolector(recolector_id: number, recogida_id: number, fechaRecogidaEstimada: Date) {
    console.log('Asignando recolector ' + recolector_id);
    this.api.asignarRecolectorRecogida(recolector_id, recogida_id, fechaRecogidaEstimada).subscribe();
    location.reload();
  }

  public completarRecogida(id: number) {
    console.log('Completar recogida ' + id);
    this.api.completarRecogida(id.toString()).subscribe();
    location.reload();
  }

  public updateRecogida(id_Recogida: number, direccion:string, id: number){
    console.log('Recogida a editarse: ' +id);
    this.api.updateRecogida(direccion, id, id_Recogida).subscribe()
    alert('Dirección guardada')
    location.reload();
  }

  public deleteRecogida(id: number){
    console.log('Recogida a cancelarse: '+id);
    this.api.deleteRecogida(id.toString()).subscribe();
    this.router.navigate(['/dashboard']);
  }

  public verPerfil(id: number){
    this.router.navigate(['/perfilrecolector', id]);
  }

 

}
