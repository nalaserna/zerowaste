// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  urlConsultarAllRecogidas: 'http://localhost:8084/getAllRecogidas',
  urlgetAllArticulos: 'http://localhost:8084/getAllArticulos',
  urlgetAllArticulosRecogida: 'http://localhost:8084/getAllArticulosRecogida',
  urladdNewArticulo: 'http://localhost:8084/addArticulo',
  urldeleteArticulo: 'http://localhost:8084/deleteArticulo',
  urlgetArticuloById: 'http://localhost:8084/getArticuloById',
  urlgetAllFundaciones: 'http://localhost:8084/getAllFundaciones',
  urladdFundacion: 'http://localhost:8084/addFundacion',
  urldeleteFundacion: 'http://localhost:8084/deleteFundacion',
  urlgetFundaciondById: 'http://localhost:8084/getFundaciondById',
  urlgetFundacionByNombre: 'http://localhost:8084/getFundacionByNombre',
  urlgetAllLocalidades: 'http://localhost:8084/getAllLocalidades',
  urlgetLocalidadById: 'http://localhost:8084/getLocalidadById',
  urlgetLocalidadByNombre: 'http://localhost:8084/getLocalidadByNombre',
  urladdRecogida: 'http://localhost:8084/addRecogida',
  urlupdateRecogida: 'http://localhost:8084/updateRecogida',
  urlasignarRecolectorRecogida: 'http://localhost:8084/asignarRecolectorRecogida',
  urlactualizarFechaRecogidaEstimada: 'http://localhost:8084/actualizarFechaRecogidaEstimada',
  urlcompletarRecogida: 'http://localhost:8084/completarRecogida',
  urldeleteRecogida: 'http://localhost:8084/deleteRecogida',
  urlgetRecogidaById: 'http://localhost:8084/getRecogidaById',
  urlgetRecogidaByDonador: 'http://localhost:8084/getRecogidaByDonador',
  urlgetRecogidaByRecolector: 'http://localhost:8084/getRecogidaByRecolector',
  urlgetRecogidaByLocalidad: 'http://localhost:8084/getRecogidaByLocalidad',
  urladdNewRecolector: 'http://localhost:8084/addRecolector',
  urlupdateRecolector: 'http://localhost:8084/updateRecolector',
  urldeleteRecolector: 'http://localhost:8084/deleteRecolector',
  urldeleteDonador: 'http://localhost:8084/deleteDonador',
  urlgetAllUsers: 'http://localhost:8084/getAllUsers',
  urladdNewDonador: 'http://localhost:8084/addDonador',
  urlupdateDonador: 'http://localhost:8084/updateDonador',
  urlgetAllDonadores: 'http://localhost:8084/getAllDonadores',
  urlgetAllRecolectoresByFundacion: 'http://localhost:8084/getAllRecolectoresByFundacion',
  urlgetAllRecolectoresLocalidad: 'http://localhost:8084/getAllRecolectoresLocalidad',
  urlgetAllRecolectoresFundacionLocalidad: 'http://localhost:8084/getAllRecolectoresFundacionLocalidad',
  urlgetUserById: 'http://localhost:8084/getUserById',
  urlgetDonadorByEmail: 'http://localhost:8084/getDonadorByEmail',
  urlgetAllRecolectores: 'http://localhost:8084/getAllRecolectores',
  urlConsultaRecogidasPendientes: 'http://localhost:8084/getRecogidasPendientes',
  urlCargarImagen: 'http://localhost:8084/cargarImagen',
  urlRecogidasByFechaCreacion: 'http://localhost:8084/getRecogidaByFecha',
  urlRecogidasByFechaDonadorRecolector: 'http://localhost:8084/getRecogidaByFechaDonadorRecolector'

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
