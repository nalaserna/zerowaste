package entities;
// Generated 19/03/2019 06:39:39 PM by Hibernate Tools 4.3.5.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Articulo generated by hbm2java
 */
@Entity
@Table(name = "articulo", catalog = "zerowaste")
public class Articulo implements java.io.Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@OneToOne(fetch = FetchType.EAGER)
	private Recogida recogida;
	
	private String nombre;
	private int cantidad;
	private String descripcion;
	private String estadoUtil;
	private String imagen1;
	private String imagen2;
	private double alto;
	private double largo;
	private double ancho;
	private double peso;

	public Articulo() {
	}

	public Articulo(int id, Recogida recogida, String nombre, int cantidad, String estadoUtil, String imagen1,
			double alto, double largo, double ancho, double peso) {
		this.id = id;
		this.recogida = recogida;
		this.nombre = nombre;
		this.cantidad = cantidad;
		this.estadoUtil = estadoUtil;
		this.imagen1 = imagen1;
		this.alto = alto;
		this.largo = largo;
		this.ancho = ancho;
		this.peso = peso;
	}

	public Articulo(int id, Recogida recogida, String nombre, int cantidad, String descripcion, String estadoUtil,
			String imagen1, String imagen2, double alto, double largo, double ancho, double peso) {
		this.id = id;
		this.recogida = recogida;
		this.nombre = nombre;
		this.cantidad = cantidad;
		this.descripcion = descripcion;
		this.estadoUtil = estadoUtil;
		this.imagen1 = imagen1;
		this.imagen2 = imagen2;
		this.alto = alto;
		this.largo = largo;
		this.ancho = ancho;
		this.peso = peso;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

/*	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_Recogida", nullable = false)*/
	
	public Recogida getRecogida() {
		return this.recogida;
	}

	public void setRecogida(Recogida recogida) {
		this.recogida = recogida;
	}

	@Column(name = "nombre", nullable = false, length = 30)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "cantidad", nullable = false)
	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	@Column(name = "descripcion", length = 300)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "estadoUtil", nullable = false, length = 12)
	public String getEstadoUtil() {
		return this.estadoUtil;
	}

	public void setEstadoUtil(String estadoUtil) {
		this.estadoUtil = estadoUtil;
	}

	@Column(name = "imagen1", nullable = false, length = 65535)
	public String getImagen1() {
		return this.imagen1;
	}

	public void setImagen1(String imagen1) {
		this.imagen1 = imagen1;
	}

	@Column(name = "imagen2", length = 65535)
	public String getImagen2() {
		return this.imagen2;
	}

	public void setImagen2(String imagen2) {
		this.imagen2 = imagen2;
	}

	@Column(name = "alto", nullable = false, precision = 22, scale = 0)
	public double getAlto() {
		return this.alto;
	}

	public void setAlto(double alto) {
		this.alto = alto;
	}

	@Column(name = "largo", nullable = false, precision = 22, scale = 0)
	public double getLargo() {
		return this.largo;
	}

	public void setLargo(double largo) {
		this.largo = largo;
	}

	@Column(name = "ancho", nullable = false, precision = 22, scale = 0)
	public double getAncho() {
		return this.ancho;
	}

	public void setAncho(double ancho) {
		this.ancho = ancho;
	}

	@Column(name = "peso", nullable = false, precision = 22, scale = 0)
	public double getPeso() {
		return this.peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

}
