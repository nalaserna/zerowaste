package repositories;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

import entities.Articulo;
import entities.Localidad;

public interface LocalidadRepository extends CrudRepository<Localidad, Long> {

	public Localidad findById(int id);
	
	public Localidad findByNombre(String nombre);
	
}
