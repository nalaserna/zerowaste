package repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import entities.Localidad;
import entities.Recogida;
import entities.Usuario;

public interface RecogidaRepository extends CrudRepository<Recogida, Long>{

	public Recogida findById(int id);
	
	public List<Recogida> findByDonador(Usuario donador);
	
	public List<Recogida> findByRecolector(Usuario recolector);
	
	public List<Recogida> findByLocalidad(Localidad localidad);
	
	public List<Recogida> findByFechaSolicitud(Date fechaSolicitud);
	
	public List<Recogida> findByDireccion(String direccion);
	
	public List<Recogida> findByFechaEstimadaEntrega(Date fechaEstimadaEntrega);
	
	public List<Recogida> findByFechaCompletada(Date fechaCompletada);
	
	public List<Recogida> findByFechaEstimadaEntregaAndRecolectorAndDonador(Date fechaEstimadaEntrega, Usuario recolector, Usuario donador);
}
