package servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Localidad;
import repositories.LocalidadRepository;

@RestController
public class LocalidadController {

	@Autowired
	private LocalidadRepository localidadRepositoryDAO;
	
	@CrossOrigin
	@RequestMapping("/getAllLocalidades")
	public Iterable<Localidad> getAllLocalidades () {
		
		Iterable<Localidad> findAll = localidadRepositoryDAO.findAll();
		
		return findAll;
	}
	
	@CrossOrigin
	@RequestMapping(path="/addLocalidad", method=RequestMethod.POST) 
	public @ResponseBody String addNewLocalidad
	(@RequestParam String nombre) {
		
		Localidad nuevaLocalidad = new Localidad();
		nuevaLocalidad.setNombre(nombre);
		localidadRepositoryDAO.save(nuevaLocalidad);
		return "Localidad Guardada";
	}
	
	@CrossOrigin
	@RequestMapping(path="/deleteLocalidad", method=RequestMethod.POST) 
	public @ResponseBody String deleteLocalidad
	(@RequestParam int id) {
		
		Localidad deleteLocalidad = localidadRepositoryDAO.findById(id);
		localidadRepositoryDAO.delete(deleteLocalidad);
		return "Localidad Eliminada";
		
	}
	
	@CrossOrigin
	@RequestMapping ("/getLocalidadById")
	public Localidad getLocalidadById (@RequestParam String id) {
		
		return localidadRepositoryDAO.findById(Integer.parseInt(id));
		
	}
	
	@CrossOrigin
	@RequestMapping ("/getLocalidadByNombre")
	public Localidad getLocalidadByNombre (@RequestParam String nombre) {
		
		return localidadRepositoryDAO.findByNombre(nombre);
		
	}
}
