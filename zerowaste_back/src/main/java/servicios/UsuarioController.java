package servicios;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Fundacion;
import entities.Localidad;
import entities.Usuario;
import repositories.FundacionRepository;
import repositories.LocalidadRepository;
import repositories.UsuarioRepository;

@RestController
public class UsuarioController {
	
	@Autowired
	private UsuarioRepository usuarioRepositoryDAO;
	
	@Autowired
	private LocalidadRepository localidadRepositoryDAO;
	
	@Autowired
	private FundacionRepository fundacionRepositoryDAO;
	
	@CrossOrigin
	@RequestMapping(path="/getAllUsers", method=RequestMethod.GET)
	public Iterable<Usuario> getAllUsers () {
		
		Iterable<Usuario> findAll = usuarioRepositoryDAO.findAll();
		
		return findAll;
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/addDonador", method=RequestMethod.POST) 
	public @ResponseBody String addNewDonador
	(@RequestParam String nombre, @RequestParam String apellido, 
	@RequestParam String email, @RequestParam String celular, 
	@RequestParam String cedula, @RequestParam String password, 
	@RequestParam String fecha_nacimiento ) throws ParseException {
		
		Date hoy = Calendar.getInstance().getTime();
		Date fechaNacimiento = new SimpleDateFormat("yyyy-MM-dd").parse(fecha_nacimiento);  
		
		Usuario nuevoDonador = new Usuario();
		nuevoDonador.setNombre(nombre);
		nuevoDonador.setApellido(apellido);
		nuevoDonador.setCedula(cedula);
		nuevoDonador.setCelular(celular);
		nuevoDonador.setCorreo(email);
		nuevoDonador.setPassword(password);
		nuevoDonador.setFechaNacimiento(fechaNacimiento);
		nuevoDonador.setFechaCreacion(hoy);
		nuevoDonador.setEstado(true);
		nuevoDonador.setTipoUsuario("Donador");
		usuarioRepositoryDAO.save(nuevoDonador);
		return "Donador Guardado";
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/addRecolector", method=RequestMethod.POST) 
	public @ResponseBody String addNewRecolector
	(@RequestParam String nombre, @RequestParam String fecha_nacimiento, 
	@RequestParam int id_fundacion, @RequestParam int id_localidad, 
	@RequestParam String apellido ) throws ParseException {
		
		Localidad localidad = localidadRepositoryDAO.findById(id_localidad);
		Fundacion fundacion = fundacionRepositoryDAO.findById(id_fundacion);
		Date hoy = Calendar.getInstance().getTime();
		Date fechaNacimiento = new SimpleDateFormat("yyyy-MM-dd").parse(fecha_nacimiento);  
		
		Usuario nuevoRecolector = new Usuario();
		nuevoRecolector.setFechaCreacion(hoy);
		nuevoRecolector.setEstado(true);
		nuevoRecolector.setNombre(nombre);
		nuevoRecolector.setApellido(apellido);
		nuevoRecolector.setFechaNacimiento(fechaNacimiento);
		nuevoRecolector.setTipoUsuario("Recolector");
		nuevoRecolector.setLocalidad(localidad);
		nuevoRecolector.setFundacion(fundacion);
		usuarioRepositoryDAO.save(nuevoRecolector);
		return "Recolector Guardado";
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/deleteRecolector", method=RequestMethod.POST) 
	public @ResponseBody String deleteRecolector
	(@RequestParam String id) {
		
		Usuario nuevoRecolector = usuarioRepositoryDAO.findById(Integer.parseInt(id));
		usuarioRepositoryDAO.delete(nuevoRecolector);
		return "Recolector Eliminado";
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/updateRecolector", method=RequestMethod.POST) 
	public @ResponseBody String updateRecolector
	(@RequestParam String id, @RequestParam String nombre, @RequestParam String fecha_nacimiento, 
			@RequestParam String id_fundacion, @RequestParam String id_localidad, 
			@RequestParam String apellido) throws ParseException {
		
		Date fechaNacimiento = new SimpleDateFormat("yyyy-MM-dd").parse(fecha_nacimiento);  
		
		Localidad localidad = localidadRepositoryDAO.findById(Integer.parseInt(id_localidad));
		Fundacion fundacion = fundacionRepositoryDAO.findById(Integer.parseInt(id_fundacion));
		Usuario nuevoRecolector = usuarioRepositoryDAO.findById(Integer.parseInt(id));
		nuevoRecolector.setNombre(nombre);
		nuevoRecolector.setApellido(apellido);
		nuevoRecolector.setFundacion(fundacion);
		nuevoRecolector.setLocalidad(localidad);
		nuevoRecolector.setFechaNacimiento(fechaNacimiento);
		usuarioRepositoryDAO.save(nuevoRecolector);
		return "Recolector Actualizado";
		
	}
	
	@CrossOrigin
	@RequestMapping(path="/updateDonador", method=RequestMethod.POST) 
	public @ResponseBody String updateDonador
	(@RequestParam String id, @RequestParam String nombre, @RequestParam String apellido, 
			@RequestParam String email, @RequestParam String celular, 
			@RequestParam String cedula, @RequestParam String password, 
			@RequestParam String fecha_nacimiento ) throws ParseException {
		
		Date fechaNacimiento = new SimpleDateFormat("dd/MM/yyyy").parse(fecha_nacimiento);  
		
		
		Usuario nuevoRecolector = usuarioRepositoryDAO.findById(Integer.parseInt(id));
		nuevoRecolector.setNombre(nombre);
		nuevoRecolector.setApellido(apellido);
		nuevoRecolector.setCorreo(email);
		nuevoRecolector.setCedula(cedula);
		nuevoRecolector.setCelular(celular);
		nuevoRecolector.setPassword(password);
		nuevoRecolector.setFechaNacimiento(fechaNacimiento);
		usuarioRepositoryDAO.delete(nuevoRecolector);
		return "Recolector Actualizado";
		
	}
	
	
	@CrossOrigin
	@RequestMapping(path="/deleteDonador", method=RequestMethod.POST) 
	public @ResponseBody String deleteDonador
	(@RequestParam String id) {
		
		Usuario nuevoDonador = usuarioRepositoryDAO.findById(Integer.parseInt(id));
		usuarioRepositoryDAO.delete(nuevoDonador);
		return "Donador Eliminado";
		
	}
	
	@CrossOrigin
	@RequestMapping (path="/getAllRecolectores", method=RequestMethod.GET)
	public Iterable<Usuario> getAllRecolectores() {
		Iterable<Usuario> recolectores= usuarioRepositoryDAO.findByTipoUsuario("Recolector");
		return recolectores;
	}
	
	@CrossOrigin
	@RequestMapping (path="/getAllDonadores", method=RequestMethod.GET)
	public Iterable<Usuario> getAllDonadores() {
		Iterable<Usuario> donadores= usuarioRepositoryDAO.findByTipoUsuario("Donador");
		return donadores;
	}
	
	@CrossOrigin
	@RequestMapping (path="/getAllRecolectoresByFundacion", method=RequestMethod.GET)
	public Iterable<Usuario> getAllRecolectoresByFundacion(@RequestParam String id_fundacion) {
		Fundacion fundacion = fundacionRepositoryDAO.findById(Integer.parseInt(id_fundacion));
		
		Iterable<Usuario> recolectores= usuarioRepositoryDAO.findByFundacionAndTipoUsuario(fundacion,"Recolector");
		return recolectores;
	}
	
	@CrossOrigin
	@RequestMapping (path="/getAllRecolectoresFundacionLocalidad", method=RequestMethod.GET)
	public Iterable<Usuario> getAllRecolectoresFundacionLocalidad(@RequestParam String id_fundacion, 
			@RequestParam String id_localidad) {
		Localidad localidad = localidadRepositoryDAO.findById(Integer.parseInt(id_localidad));
		Fundacion fundacion = fundacionRepositoryDAO.findById(Integer.parseInt(id_fundacion));
		
		Iterable<Usuario> recolectores= usuarioRepositoryDAO.findByFundacionAndLocalidad(fundacion,localidad);
		return recolectores;
	}
	
	@CrossOrigin
	@RequestMapping (path="/getAllRecolectoresLocalidad", method=RequestMethod.GET)
	public Iterable<Usuario> getAllRecolectoresLocalidad(@RequestParam String id_localidad) {
		Localidad localidad = localidadRepositoryDAO.findById(Integer.parseInt(id_localidad));
		
		Iterable<Usuario> recolectores= usuarioRepositoryDAO.findByLocalidad(localidad);
		return recolectores;
	}
	
	@CrossOrigin
	@RequestMapping ("/getUserById")
	public Usuario getUserById (@RequestParam String id) {
		
		return usuarioRepositoryDAO.findById(Integer.parseInt(id));
		
	}
	
	@CrossOrigin
	@RequestMapping ("/getDonadorByEmail")
	public Usuario getDonadorByEmail (@RequestParam String email) {
		
		return usuarioRepositoryDAO.findByCorreo(email);
		
	}
	
	
}
